import React, { useState, useEffect } from 'react';
import { Form, Button, Col, Row } from 'react-bootstrap';
import Router from 'next/router';
import Head from 'next/head';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')

	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Function to register user
    function registerUser(e) {

        e.preventDefault();

        // Check for duplicate email in database first
        fetch(`https://rocky-brook-64027.herokuapp.com/api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
            .then(res => res.json())
            .then(data => {

                // If no duplicates found
                if (data === false) {

                    fetch(`https://rocky-brook-64027.herokuapp.com/api/users/`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password1
                        })
                    })
                        .then(res => res.json())
                        .then(data => {

                            // Registration successful
                            if (data === true) {
                                // Redirect to login
                                Router.push('/login')
                            } else {
                                // Error in creating registration, redirect to error page
                                Router.push('/error')
                            }

                        })

                } else { // Duplicate email found
                    Router.push('/error')
                }

            })
    }

	useEffect(() => {
		// validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}


	}, [ email, password1, password2 ])

	return (
		<React.Fragment>
			<Head>
				<title>Registration</title>
			</Head>
			<Form onSubmit={e => registerUser(e)}>

			<Form.Group>
				<Row>
					<Col>
						<Form.Label>First Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="First Name"
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
							required
						/>
					</Col>
					<Col>
						<Form.Label>Last Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Last Name"
							value={lastName}
							onChange={e => setLastName(e.target.value)}
							required
						/>
					</Col>
				</Row>
			</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{/* conditionally render submit button based on isActive state */}
				{ isActive
					? 
					<Button className="bg-primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button className="bg-danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
				
			</Form>
		</React.Fragment>
	)
}