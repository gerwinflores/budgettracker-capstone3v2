import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../helpers';
import moment from 'moment';
import { Alert } from 'react-bootstrap';

export default function breakdown(){

	const [chartLabels, setChartLabels] = useState([]);
	const [amounts, setAmounts] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [records, setRecords] = useState([]);

	console.log(startDate)
	console.log(endDate)

	const data = {
		labels: chartLabels,
		datasets: [{
			data: amounts,
			backgroundColor: bgColors,
			hoverBackground: bgColors
		}]
	}

	// useEffect(() =>{

	// 	const options = {
	// 		headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
	// 	};
		
	// 	fetch(`http://localhost:4000/api/users/details`, options)
	// 	.then(res => res.json())
 //        .then(data => {
        	
 //        	setRecords(data.records)

 //        })

	// },[])

	useEffect(() =>{

		let tempLabels = [];
        	records.forEach(record => {
        		if (!tempLabels.find(label => label === record.categoryName)) {
        			tempLabels.push(record.categoryName)

        		}
        		
    		})

        	setChartLabels(tempLabels)
        	
        	setAmounts(
        		tempLabels.map(label =>{
	        		let total = 0
	        		records.forEach(record => {
	        			if(record.categoryName ===  label){
	        				total += record.amount 
	        			}
	        		})

	        		return total
        		})

        		
        	)

        	setBgColors(records.map(() => `#${colorRandomizer()}`))

	},[ records ])

	useEffect(() =>{

		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		};
		
		fetch(`https://rocky-brook-64027.herokuapp.com/api/users/details`, options)
		.then(res => res.json())
        .then(data => {
        	
        	let filteredRecords = data.records.map(record => {
				if( (moment(record.date).format('YYYY-MM-DD') >= moment(startDate).format('YYYY-MM-DD')) && (moment(record.date).format('YYYY-MM-DD') <= moment(endDate).format('YYYY-MM-DD'))){
					return record
				} else {
					return null
				}
			})
			let finalRecords = filteredRecords.filter(record => record !== null)
				
			if(finalRecords.length > 0){
				setRecords(finalRecords)
			} else {
				setRecords([])
			}

        })

		


	},[ startDate, endDate ])

	return(
		<React.Fragment>
			<input type="date" onChange={(e) => setStartDate(e.target.value)}/>
			<input type="date" onChange={(e) => setEndDate(e.target.value)}/>
			{startDate !== '' && endDate !== '' && records.length > 0
			?
			<Pie data={data} />
			:
			<Alert variant="info">
				Please select a valid date
			</Alert>
			}
			
		</React.Fragment>
	)

}